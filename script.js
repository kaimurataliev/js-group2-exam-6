function showEditModal() {
    $('#editModal').fadeIn();
}

function hideEditModal () {
    $('#editModal').hide();
}

function showFollowUserModal() {
    $('#followUserModal').fadeIn();
}

function hideFollowUserModal () {
    $('#followUserModal').hide();
}

var url = 'http://146.185.154.90:8000/blog/kai.none123@gmail.com/profile';


function changeInfoOnServer () {
    var firstName = $('#firstNameModal').val();
    var lastName = $('#lastNameModal').val();
    $.ajax({
        method: 'POST',
        url: url,
        data: {
            firstName: firstName,
            lastName: lastName
        },
        success: function (data) {
            $('#firstName').text(data.firstName);
            $('#lastName').text(data.lastName);
        }
    });
    hideEditModal();
}

$('#editUser').on('click', (e) => {
    e.preventDefault();
    showEditModal();
});

$('#saveProfile').submit((e) => {
    e.preventDefault();
    changeInfoOnServer();
});

$.ajax ({
    url: url
})
    .then((response) => {
        $('#firstName').text(response.firstName);
        $('#lastName').text(response.lastName);
    });


$('#followUser').on('click', (e) => {
    e.preventDefault();
    showFollowUserModal();
});


$('#add').submit((e) => {
    var email = $('#email').val();
    e.preventDefault();
    var url = 'http://146.185.154.90:8000/blog/kai.none123@gmail.com/subscribe';
    $.ajax({
        method: 'POST',
        url: url,
        data: {email: email}
    });
    hideFollowUserModal()
});



$('#sendBtn').on('click', (e) => {
    e.preventDefault();
    var message = $('#input').val();
    $.ajax({
        method: 'POST',
        url: 'http://146.185.154.90:8000/blog/kai.none123@gmail.com/posts',
        data: {message: message},
        success: addDataOnHtml()
    })
});

function addDataOnHtml () {
    $.ajax({
        url: 'http://146.185.154.90:8000/blog/kai.none123@gmail.com/posts',
        success: function (data) {
            var messageDiv = $('<div id="messageDiv">');
            data.forEach(function (key) {
                var headerBox = $('<div class="messageBlock">');
                var messageText = $('<p id="messageText">');
                var userSaid = $('<span id="userSaid">');
                var messageDate = $('<span id="date">').text(key.datetime + ' ');
                userSaid.text(key.user.firstName + ' ' + key.user.lastName + ' said:');
                messageText.text(key.message);
                headerBox.append(userSaid, messageText, messageDate);
                messageDiv.append(headerBox)
            });
            $('#messagesBox').html(messageDiv);
        }
    });
}

setTimeout(addDataOnHtml, 2000);



